package oneToOne;

import intro.Animal;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class OneToOneTest {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();

        configuration.configure();

        //1 вариант - сохраняем только персону. Паспорт - не сохранился, персона - сохранилась. В колонке passport_id - null
        //2 вариант - сохранем и персону, и паспорт. И паспорт, и персона - сохранены, но в персоне нет ссылки на паспорт - в колонке passport_id - null
        //3 вариант - сохранем и паспорт, и персону. И паспорт, и персона - сохранены, в персоне есть ссылка на паспорт.
        try (SessionFactory sessionFactory = configuration.buildSessionFactory()){
            Session session = sessionFactory.openSession();

            Passport passport = Passport.builder()
                    .series("1111")
                    .numbers("222222")
                    .build();

            Person person = Person.builder()
                    .firstName("Oleg")
                    .lastName("Igonin")
                    .patronymic("Leonidovich")
                    .passport(passport)
                    .build();

            session.save(passport);
            session.save(person);

            Person newPerson = session.get(Person.class, 1L);
            session.close();
        }
    }
}
