package manyToMany;

import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;

import java.util.List;

public class StudentRepositoryImpl implements StudentRepository {

    private final Session session;

    public StudentRepositoryImpl(Session session) {
        this.session = session;
    }

    @Override
    public List<Student> getAll() {
        Query<Student> studentNativeQuery = session.createQuery("from Student", Student.class);
        return studentNativeQuery.getResultList();
    }
}
