package manyToMany.bookAndAuthors;

import manyToMany.Student;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class BookRepositoryImpl implements BookRepository {
    private final Session session;

    public BookRepositoryImpl(Session session) {
        this.session = session;
    }

    @Override
    public List<Book> getAll() {
        Query<Book> bookNativeQuery = session.createQuery("from Book", Book.class);
        return bookNativeQuery.getResultList();
    }
}
