package manyToMany.bookAndAuthors;

import manyToMany.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        Configuration configuration = new Configuration();

        configuration.configure();

        try (SessionFactory sessionFactory = configuration.buildSessionFactory()) {
            Session session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();

            select(session);

            transaction.commit();

            session.close();
        }
    }

    public static void select(Session session){
        AuthorRepository authorRepository = new AuthorRepositoryIml(session);
        BookRepository bookRepository = new BookRepositoryImpl(session);

        System.out.println("Books:");
        List<Book> books = bookRepository.getAll();
        System.out.println(books);

        System.out.println("Authors:");
        List<Author> authors = authorRepository.getAll();
        System.out.println(authors);
    }

    public static void insert(Session session) {
        Author gabriel = Author.builder()
                .name("Габриэль Гарсиа Маркес")
                .build();

        Author mercedes = Author.builder()
                .name("Мерседес Барбара")
                .build();

        session.save(gabriel);
        session.save(mercedes);

        Book alone = Book.builder()
                .name("Сто лет одиночества")
                .authors(List.of(gabriel, mercedes))
                .build();

        Book autumnPatrol = Book.builder()
                .name("Осенний патруль")
                .authors(List.of(gabriel))
                .build();

        session.save(alone);
        session.save(autumnPatrol);

    }
}
