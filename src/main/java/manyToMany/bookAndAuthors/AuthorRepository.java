package manyToMany.bookAndAuthors;

import manyToMany.Student;

import java.util.List;

public interface AuthorRepository {

    List<Author> getAll();
}
