package manyToMany.bookAndAuthors;

import manyToMany.Teacher;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class AuthorRepositoryIml implements AuthorRepository {
    private final Session session;

    public AuthorRepositoryIml(Session session) {
        this.session = session;
    }

    @Override
    public List<Author> getAll() {
        Query<Author> authorNativeQuery = session.createQuery("from Author", Author.class);
        return authorNativeQuery.getResultList();
    }
}
