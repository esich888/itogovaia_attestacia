package manyToMany.bookAndAuthors;

import manyToMany.Student;

import java.util.List;

public interface BookRepository {

    List<Book> getAll();
}
