package manyToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    @ManyToMany
    @JoinTable(name = "student_teacher",
               joinColumns = @JoinColumn(name = "student_id"),
               inverseJoinColumns = @JoinColumn(name = "teacher_id"))
    private List<Teacher> teachers;

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Teacher teacher : teachers) {
            stringBuilder.append("[Teacher ")
                    .append(teacher.getId())
                    .append(" ")
                    .append(teacher.getName())
                    .append("]");
        }
        return "[Student " + id + " " + name + " (" + stringBuilder +  ")]";
    }

}
