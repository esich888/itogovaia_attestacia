package manyToMany;

import java.util.List;

public interface TeacherRepository {

    List<Teacher> getAll();

}
