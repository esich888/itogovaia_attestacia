package oneToMany;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AirPlane {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;
    //fetch = FetchType.LAZY - ленивая загрузка, данные загрузятся только тогда, когда будет обращение к этому полю
    //fetch = FetchType.EAGER - агрессивная загрузка, данные загрузятся сразу
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "airPlane")
    private List<Passenger> passengers;
}
