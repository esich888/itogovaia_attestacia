package intro;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class IntroTest {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();

        configuration.configure();

        try (SessionFactory sessionFactory = configuration.buildSessionFactory()){
            Session session = sessionFactory.openSession();

            Animal animal = Animal.builder()
                    .type("Cat")
                    .color("Black")
                    .classAnimal("Cat Family")
                    .build();

            session.save(animal);
            session.close();
        }
    }
}
